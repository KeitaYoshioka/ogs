Choice for AT1 model (phasefield_model="AT1") or AT2 model (phasefield_model="AT2") of phase-field.

For further information, refer to:
Tann{\'e'} E et al (2018). Crack nucleation in variational phase-field models of brittle fracture. J Mech Phys Solids, 110:80-99
\cite tanne2018crack
